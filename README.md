# TeachServer

Un petit serveur qui utilise Python 3 avec le framework Bottle et qui permet à un professeur de partager des questionnaires et des fichiers avec ses élèves quand ils sont connectés sur le même réseau.
Vous devez donc :
 - être administrateur de votre serveur linux
 - avoir installé python 3, Bottle, et gunicorn
 - autoriser les connexions entrantes dans votre firewall (au moins le port 8080)



## Utilisation :

Téléchargez ce dossier

A partir de la console placez vous dans le dossier et lancer la commande "python3 server.py"

Vous pouvez maintenant accéder à l'interface administrateur en tapant "IP de votre poste:8080" dans la barre d'adresse de votre navigateur internet.

Le serveur permet de :
 - Créer des questionnaires
 - Recevoir des fichiers de vos élèves (ils seront renommés et horodatés)
 - Partager des fichiers

 Les élèves peuvent ce connecter à ce serveur en tapant "IP de votre poste:8080" (exemple:192.168.1.31:8080) dans la barre d'adresse de leur navigateur internet.
 

 Pour quitter le programme placez vous dans la console et tapez "Ctrl-C".
 





