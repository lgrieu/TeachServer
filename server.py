from bottle import route, get, post, request, run, static_file, auth_basic, redirect
import os
import shutil
import sys
import sqlite3
from string import Template
from time import strftime
import hashlib

admin_name = "admin"
admin_password = 'admin'

html = """
<!DOCTYPE html>
  <html lang="fr">
  <head>
  <title>SimpleShare</title>
  <link rel="icon" type="image/x-icon" href="/images/favicon.ico">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <style>
    {css}
  </style>
  </head>
  <body>
      <h2>{title}</h2>
      {header}
    <div class="flex-container">
      {body}
    </div>
      {footer}
    <script>
      {script}
    </script>
  </body>
</html>
"""
css = r"""
    * {
  box-sizing: border-box;
}
  table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid black;
  text-align: left;
  padding: 8px;
}
tr:nth-child(even) {
  background-color: #dddddd;
}
.flex-container {
  display: flex;
  flex-direction: row;
  text-align: center;
}
.flex-item {
  background-color: #f1f1f1;
  margin:0.5em;
  padding:1em;
  border: none;
  border-radius: 5px;
}
fieldset{
  border-radius: 7px;
  border: none;
  background-color: #ffe36c;
  padding:1em ;
  }
legend{
  margin-left:0;
  border-radius: 7px;
  border: none;
  background-color: #ffe36c;
  padding:1em ;
}
input[type="file"] {
  display:none;
}
.txt {
  width:100%;
  display:block;
  padding:1em;
  border: none;
  border-radius: 5px;
  text-align: center;
  font-weight:bold;
  font-size:1em;
  margin: auto;
}
.txt:hover {
  border: 1px solid #4CAF50;
}
.Btn {
  width:100%;
  display:block;
  padding:1em;
  background-color: #4CAF50;
  border: none;
  border-radius: 5px;
  color: white;
  text-align: center;
  text-decoration: none;
  font-weight:bold;
  font-size:1em;
  margin: auto;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
}
.Btn:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
}

.SuccesMsg {
  display:inline-block;
  padding:1em;
  border: none;
  border-radius: 5px;
  color: #270;
  background-color: #DFF2BF;
  text-align: center;
  text-decoration: none;
  font-weight:bold;
  font-size:1em;
  margin: auto;
}
.ErrorMsg {
  display:inline-block;
  padding:1em;
  border: none;
  border-radius: 5px;
  color: #D8000C;
  background-color: #FFBABA;
  text-align: center;
  text-decoration: none;
  font-weight:bold;
  font-size:1em;
  margin: auto;
}
/* Responsive layout - makes a one column-layout instead of two-column layout */
@media (max-width: 800px) {
  .flex-container {
    flex-direction: column;
  }
}
"""
admin_link = """
    <a href='/admin' >&#128450;</a>
    """
download_form = """
      <div class="flex-item" style="flex:{width};">

            <form id='Form' action="{action}" method="post" enctype="multipart/form-data" >

            <h3>{title}</h3>
                <input style="display: none;"  name="upload" id="upload" type="file"
                onchange="document.getElementById('FileMsg').innerText = this.files[0].name;" >

                <label for="upload" class="Btn" ><span id="FileMsg"  >1. Choisir un fichier</span></label>
                <br>
                <input type="text" class="txt" id="name" name="name"
                required placeholder="2. Définir un nom de fichier">
                <br>
                <div id='SendBtn' class="Btn" >3. Envoyer le fichier</div>
            </form>
            <br>
            <div id='msg'></div>
            <br>
        </div>

  """
download_form_script = r"""
          document.getElementById("SendBtn").addEventListener("click", SendPostRequest);

          function SendPostRequest() {
            let Form = document.getElementById("Form");
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                document.getElementById("msg").innerHTML = this.responseText;
              }
            };
          xhttp.open(Form.method, Form.action, true);
          xhttp.send(new FormData(Form));
}
"""
admin_script = r"""
            document.getElementById("SendBtn").addEventListener("click",function(){
              setTimeout(get_back, 1000);

              },false);

            function get_back(){
              let back_link = document.createElement('a');
              document.body.appendChild(back_link);
              back_link.href = "/admin";
              back_link.click();
              back_link.remove();
            }

            function delete_msg(url) {
              let msg = "Confirmer la suppression ! Toutes les données seront perdues !";
              if (confirm(msg)) {
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    get_back();
                  }
                };
                xhttp.open("GET", url);
                xhttp.send();

              }
            }
            function tableToCSV(id) {
              let table = document.getElementById(id);
              let csv_data = [];
              let rows = table.getElementsByTagName('tr');
              for (let i = 0; i < rows.length; i++) {
                  let cols = rows[i].querySelectorAll('td,th');
                  let csvrow = [];
                  for (var j = 0; j < cols.length; j++) {
                      csvrow.push(cols[j].innerHTML);
                  }
                  csv_data.push(csvrow.join(","));
              }
              csv_data = csv_data.join('\n');

              let blob = new Blob([csv_data], {type: 'text/csv'});
              let a = document.createElement('a');
              document.body.appendChild(a);
              a.download = "export.csv";
              a.href = window.URL.createObjectURL(blob);
              a.click();
              a.remove();
              window.URL.revokeObjectURL(a);
            }
          """
flex_div = """
        <div class="flex-item" style="flex:{width};">
        <h3>{title}</h3>
          {content}
        </div>
        """
succes_msg = "<div class='SuccesMsg'>&#9989; Bien reçu !</div>"

file_error_msg = "<div class='ErrorMsg'>&#9940; vous devez choisir un fichier !</div>"

name_error_msg = "<div class='ErrorMsg'>&#9940; votre nom ne peut contenir que des lettres !</div>"

empty_folder_msg = "<div class='ErrorMsg'>&#9940; Le dossier est vide !</div>"

database_error_msg = "<div class='ErrorMsg'>&#9940; Opération annulée, erreur de base de données !</div>"

root_link = "<a href='/' class='Btn' style='display:inline-block' >Acceuil</a><br><br>"

formbuilder_link = "<a href='https://lgrieu.gitlab.io/SimpleFormBuilder/' target='_blank' class='Btn' style='display:inline-block' >Editer un formulaire</a><br><br>"

script_path = os.getcwd()

def get_dir_path(dir_root, dir_name):
    dir_path = os.path.join(dir_root, dir_name)
    if not os.path.isdir(dir_path):
      try:
        os.mkdir(dir_path)
      except OSError as error:
        sys.exit(str(error))
    return dir_path
    
uploads_path = get_dir_path(script_path, "uploads")
shared_path = get_dir_path(script_path, "shared")
databases_path =  get_dir_path(script_path, "databases")
images_path =  get_dir_path(script_path, "images")

def do_upload(admin_mode):
  msg = ""
  name   = request.forms.get('name')
  upload     = request.files.get('upload')

  if upload and name.isalpha():
    file_name, ext = os.path.splitext(upload.filename)
    date = strftime("%d_%m_%Y")
    date_dir = get_dir_path(uploads_path, date)
    time = strftime("%H-%M-%S")

    if admin_mode == True:
      new_filename = name + ext
      file_path = os.path.join(shared_path,new_filename)
    else:
      new_filename = name + "_" + time + ext
      file_path = os.path.join(date_dir,new_filename)

    upload.save(file_path)
    msg = succes_msg

  elif not upload:
    msg = file_error_msg

  elif not name.isalpha():
    msg = name_error_msg

  return  msg

def list_files(dir_name,admin_mode):
    dir_path = get_dir_path(script_path, dir_name)
    file_list = os.listdir(dir_path)
    if not file_list:
      return empty_folder_msg
    else:
      files = ""
      for filename in file_list:
        file_path = os.path.join(dir_path, filename)
        file_url = os.path.join("/" + dir_name + "/", filename)
        if admin_mode == True:
          files += """
        <a href='{url}' target='_blank' class='Btn'>{filename}</a>
       <button type="button" onclick="delete_msg('/admin?file_to_delete={file_path}')">
       &#128465;
       </button>
       <br>
          """.format(url = file_url, filename = filename, file_path = file_path)
        else:
          files += """
          <a href='{url}' target='_blank' class='Btn'>{filename}</a><br>
          """.format(url = file_url, filename = filename)

      return files

def list_uploads():
    dirs_list = os.listdir(uploads_path)
    if not dirs_list:
      return empty_folder_msg
    else:
      dirs_links = ""
      for dir_name in dirs_list:
        dir_path = os.path.join(uploads_path, dir_name)
        dir_url = '/uploads/'+ dir_name + '/'
        dirs_links += """
        <fieldset>
          <legend>
            {dir_name}
            <button type="button" onclick="delete_msg('/admin?file_to_delete={dir_path}')">&#128465;</button>
          </legend>
       <br>
       {file_list}
       </fieldset>
          """.format(dir_url = dir_url, dir_name = dir_name, dir_path = dir_path, file_list = list_files('uploads/' + dir_name,True))
    return dirs_links

@route('/shared/<filepath:path>')
def shared(filepath):
    return static_file(filepath, root= "shared/")

@route('/images/<filepath:path>')
def images(filepath):
    return static_file(filepath, root= "images/")

@route('/uploads/<filepath:path>')
def uploads(filepath):
    return static_file(filepath, root= "uploads/")

@route('/uploads/*/<filepath:path>')
def uploads_files(filepath):
    return static_file(filepath, root='/uploads/*/')

@route('/')
def index():
  body = download_form.format(width = "45%",title = "Envoyer un fichier", action = "/user_upload" ) + flex_div.format(width = "45%",title = "Ressources", content = list_files("shared",False))
  return  html.format(css = css,title = "Serveur de partage", header = admin_link, body = body,footer = "", script = download_form_script)

@route('/user_upload', method='POST')
def user_upload():
  return do_upload(False)



@route('/form_handle', method='POST')
def form_handle():
    all_questions = ""
    all_answers = ""
    columns_names = ""
    form_handle_output = ""
    table_name = request.forms.get('Questionnaire_ID')

    for index, question_name in enumerate(request.forms):
      multiple_answers = request.forms.getall(question_name)
      answer = ""
      if len(multiple_answers) >1:
        for a in multiple_answers:
          answer += " / " + str(a)
      else:
        answer = request.forms.get(question_name)

      if index== len(request.forms)-1:
        all_questions += question_name
        columns_names += question_name + " TEXT"
        all_answers += "'" + answer + "'"
      else:
        all_questions += question_name + " , "
        columns_names += question_name + " TEXT,"
        all_answers += "'" + answer + "', "
    try:
      connection = sqlite3.connect("databases/answers.db")
      cursor = connection.cursor()

      create_table = "CREATE TABLE IF NOT EXISTS {table} ({columns})".format(table = table_name ,
                                                                           columns = columns_names)

      insert_row = "INSERT INTO {table}({questions}) VALUES ({answers})".format(table = table_name ,
                                                                               questions = all_questions, answers = all_answers )
      cursor.execute(create_table)
      cursor.execute(insert_row)

    except sqlite3.Error as error:
      msg = database_error_msg

    finally:

      if connection:

        connection.commit()
        connection.close()
        msg = succes_msg

    return  html.format(css = css,title = "" ,header = root_link, body = msg,footer = "", script = "")


def check_pass(username, password):
    password_to_verify = hashlib.sha256(bytes(password.encode('utf-8'))).hexdigest()
    hashed_password = hashlib.sha256(bytes(admin_password.encode('utf-8'))).hexdigest()
    return username == admin_name and password_to_verify == hashed_password

@route('/admin_upload', method='POST')
def admin_upload():
  return do_upload(True)

@route('/admin', method='GET')
@auth_basic(check_pass)
def admin():
  msg = ""
  tables_div = "<div>"
  delete_file_query = request.query.get('file_to_delete')
  if delete_file_query:
    try:
      if os.path.isfile(delete_file_query) or os.path.islink(delete_file_query):
        os.remove(delete_file_query)
      elif os.path.isdir(delete_file_query):
        shutil.rmtree(delete_file_query)
    except OSError as error:
      msg = "<div class='ErrorMsg'>&#9940; " + str(error) + "</div>"

  delete_table_query = request.query.get('table_to_delete')
  if delete_table_query:
    try:
      connection = sqlite3.connect("databases/answers.db")
      cursor = connection.cursor()
      drop = "DROP TABLE {table}".format(table = delete_table_query)
      connection.execute(drop)

    except sqlite3.Error as error:
      msg = "<div class='ErrorMsg'>&#9940; " + str(error) + "</div>"

    finally:
      if connection:
        connection.commit()
        connection.close()
    redirect('/admin')
  try:
    connection = sqlite3.connect("databases/answers.db")
    cursor = connection.cursor()

    sql_tables_query = "SELECT name FROM sqlite_master WHERE type='table';"

    cursor.execute(sql_tables_query)

    sql_tables = cursor.fetchall()
    for tables in sql_tables:
      for table_name in tables:
        tables_div += "<h3>Réponses reçues</h3>"
        tables_div += "<h4>" + str(table_name) + "</h4>"
        tables_div += "<table id='" + str(table_name) + "' >"
        select = "SELECT * FROM {table}".format(table = table_name)
        all_rows = cursor.execute(select)
        col_names = cursor.description
        tables_div += "<tr>"
        for name in col_names:
          tables_div += "<th>" + str(name[0]) + "</th>"
        tables_div += "</tr>"
        for row in all_rows.fetchall():
          tables_div += "<tr>"
          for rc in row:
            tables_div += "<td>" + str(rc) + "</td>"
          tables_div += "</tr>"
        tables_div += "</table>"
        tables_div += """
        <button type="button"
        onclick="delete_msg('/admin?table_to_delete={table}')">&#128465;</button>
        """.format(table = table_name)
        tables_div += """
        <button type="button"
        onclick="tableToCSV('{table}')" >csv</button>
        """.format(table = table_name)
        tables_div += "</div>"
  except sqlite3.Error as error:
    msg = "<div class='ErrorMsg'>&#9940; " + str(error) + "</div>"

  finally:

    if connection:

      connection.commit()
      connection.close()

  body = download_form.format(width = "25%",title = "Partager une ressource", action = "/admin_upload") + flex_div.format(width = "25%",title = "Ressources partagées", content = list_files("shared",True)) + flex_div.format(width = "25%",title = "Fichiers reçus", content = list_uploads())
  return  html.format(css = css,title = "Administration du serveur", header = root_link , body = body,footer = msg + formbuilder_link + tables_div, script = download_form_script + admin_script)

if __name__ == "__main__":
  #run(reloader=True)
  run(server='gunicorn', host='0.0.0.0', port=8080)
